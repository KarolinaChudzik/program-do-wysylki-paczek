liczba_kg_wyslanych = 0
waga_paczki = 0
paczka_nr = 0
najlzejsza_paczka_nr = 0
najlzejsza_paczka_waga = 20
print("Podaj max ilosc elementow do wyslania:")
max_ilosc = int(input())
for nr_elementu in range(max_ilosc):
    print("Podaj wage towaru:")
    waga_towaru = int(input())
    if waga_towaru <= 0:
        break
    elif 1 <= waga_towaru <= 10:
        liczba_kg_wyslanych += waga_towaru
        if waga_paczki + waga_towaru <= 20:
            waga_paczki += waga_towaru
        else:
            paczka_nr += 1
            if waga_paczki < najlzejsza_paczka_waga:
                najlzejsza_paczka_waga = waga_paczki
                najlzejsza_paczka_nr = paczka_nr
            waga_paczki = waga_towaru
    else:
        print("Podano nieprawidlowa wage towaru")
        break
paczka_nr += 1
if waga_paczki < najlzejsza_paczka_waga:
    najlzejsza_paczka_waga = waga_paczki
    najlzejsza_paczka_nr = paczka_nr
suma_pustych_kg = paczka_nr * 20 - liczba_kg_wyslanych
print("Liczba wyslanych paczek to: {}".format(paczka_nr))
print("Liczba wyslanych kilogramow to: {}".format(liczba_kg_wyslanych))
print("Suma pustych kg to: {}". format(suma_pustych_kg))
print("Najwiecej pustych miala paczka o wadze {} kg, nr {}, ilosc pustych kg: {}.".format(najlzejsza_paczka_waga, najlzejsza_paczka_nr, (20 - najlzejsza_paczka_waga)))